#pragma once

#include <Shifty.h>
#include "settings.h"

namespace TeamShapeshift { namespace SlaveFirmware
{
    class BarMotor
    {
        private:
            Shifty *shiftRegister;
            ShiftBit leftRotationBit;
            ShiftBit rightRotationBit;

        public:
            BarMotor(
                Shifty *shiftRegister,
                ShiftBit leftRotationBit,
                ShiftBit rightRotationBit
            );
            ~BarMotor();

            void rotateLeft();
            void rotateRight();
            void stop();
    };
}}
