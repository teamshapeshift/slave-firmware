# Shapeshifter (Firmware)
_Shifting shapes like any other day!_

## Pre-requisites
- Hardware
    - Arduino Mega 2560
- Software
    - A code editor (VS Code recommended)
    - PlatformIO (can be installed through VS Code Extensions)
