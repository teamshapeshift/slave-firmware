#pragma once

#include <Multiplexer.h>
#include "settings.h"
#include "BarMotor.h"

using PedZed::ArduinoUtils::Multiplexer;

namespace TeamShapeshift { namespace SlaveFirmware
{
    class Bar
    {
        public:
            Bar(
                BarMotor *,
                Multiplexer *,
                MultiplexerBit startCalibrationButtonBit,
                MultiplexerBit positionMeterBit
            );
            ~Bar();

            void calibrate();
            void moveToPosition(BarPosition);
            void moveToStartPosition();
            void moveToEndPosition();

            bool isStartCalibrationButtonTriggered();

            BarPosition getPosition();

        private:
            BarMotor *motor;
            Multiplexer *multiplexer;

            MultiplexerBit startCalibrationButtonBit;
            MultiplexerBit positionMeterBit;

            BarPosition startPosition;
            BarPosition endPosition;
            BarPosition currentPosition;

            bool hasAlreadyMeteredPosition;

            bool hasReachedPosition(BarPosition);
            bool hasReachedStart();
            bool hasReachedEnd();

            AnalogValue positionMeterValue();
    };
}}
