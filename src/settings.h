#pragma once

#include <stdint.h>
#include <Arduino.h>

#define BAR_AMOUNT 4

/**
 * The bus address for i2c.
 */
using I2cBus = uint8_t;

/**
 * The hardware pin on a microcontroller
 */
using Pin = uint8_t;

/**
 * The bit address of a shift register.
 */
using ShiftBit = uint8_t;

/**
 * The bit address of a multiplex register.
 */
using MultiplexerBit = uint8_t;

/**
 * An analog value (0-1023).
 */
using AnalogValue = uint16_t;

/**
 * The position of a bar.
 */
using BarPosition = uint16_t;

/**
 * The bus address for i2c.
 *
 * Reserved addresses: 0-8, 120-128.
 */
extern I2cBus i2cBus;

extern Pin multiplexSelectPins[3];
extern Pin multiplexInputPin;

extern Pin shiftRegisterDataPin;
extern Pin shiftRegisterClockPin;
extern Pin shiftRegisterLatchPin;
extern Pin shiftRegisterOutputEnablePin;
extern Pin shiftRegisterMasterResetPin;
extern ShiftBit shiftRegisterBitAmount;

extern uint8_t barAmount;

extern AnalogValue calibrationButtonThreshold;
// TODO: Perhaps rename to something about revelation/rotation
extern AnalogValue positionMeterThreshold;
