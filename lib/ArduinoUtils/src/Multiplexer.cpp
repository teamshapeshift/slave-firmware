#include <Arduino.h>
#include "Multiplexer.h"

using PedZed::ArduinoUtils::Multiplexer;

Multiplexer::Multiplexer(Pin *selectPins, Pin inputPin)
    : selectPins(selectPins), inputPin(inputPin)
{
    for (uint8_t selectPinIndex = 0; selectPinIndex <= SELECT_PIN_AMOUNT; selectPinIndex++) {
        pinMode(selectPins[selectPinIndex], OUTPUT);
    }
}

Multiplexer::~Multiplexer()
{
    //
}

void Multiplexer::update()
{
    for (MultiplexBit bit = 0; bit < BIT_AMOUNT; bit++) {
        digitalWrite(selectPins[0], HIGH && (bit & B00000001));
        digitalWrite(selectPins[1], HIGH && (bit & B00000010));
        digitalWrite(selectPins[2], HIGH && (bit & B00000100));
        values[bit] = analogRead(inputPin);
    }
}

uint16_t Multiplexer::readBit(MultiplexBit bit)
{
    return values[bit];
}
