#include <Arduino.h>
#include "BarMotor.h"

using TeamShapeshift::SlaveFirmware::BarMotor;

BarMotor::BarMotor(
    Shifty *shiftRegister,
    ShiftBit leftRotationBit,
    ShiftBit rightRotationBit
) : shiftRegister(shiftRegister),
    leftRotationBit(leftRotationBit),
    rightRotationBit(rightRotationBit)
{
    //
}

BarMotor::~BarMotor()
{
    //
}

void BarMotor::rotateLeft()
{
    shiftRegister->batchWriteBegin();
    shiftRegister->writeBit(leftRotationBit, HIGH);
    shiftRegister->writeBit(rightRotationBit, LOW);
    shiftRegister->batchWriteEnd();
}

void BarMotor::rotateRight()
{
    shiftRegister->batchWriteBegin();
    shiftRegister->writeBit(leftRotationBit, LOW);
    shiftRegister->writeBit(rightRotationBit, HIGH);
    shiftRegister->batchWriteEnd();
}

void BarMotor::stop()
{
    shiftRegister->batchWriteBegin();
    shiftRegister->writeBit(leftRotationBit, LOW);
    shiftRegister->writeBit(rightRotationBit, LOW);
    shiftRegister->batchWriteEnd();
}
