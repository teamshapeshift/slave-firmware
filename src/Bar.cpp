#include <Arduino.h>
#include "Bar.h"

using TeamShapeshift::SlaveFirmware::Bar;

Bar::Bar(
    BarMotor *motor,
    Multiplexer *multiplexer,
    MultiplexerBit startCalibrationButtonBit,
    MultiplexerBit positionMeterBit
) : motor(motor),
    multiplexer(multiplexer),
    startCalibrationButtonBit(startCalibrationButtonBit),
    positionMeterBit(positionMeterBit)
{
    startPosition = 0;
    endPosition = 50;
    hasAlreadyMeteredPosition = false;
}

Bar::~Bar()
{
    //
}

void Bar::calibrate()
{
    Serial.println("Calibrating...");
    moveToStartPosition();
    currentPosition = startPosition;
}

void Bar::moveToStartPosition()
{
    while (!isStartCalibrationButtonTriggered()) {
        motor->rotateLeft();
        currentPosition--;
        multiplexer->update();
    }

    // TODO: Check if stop() is necessary
    motor->stop();
}

bool Bar::isStartCalibrationButtonTriggered()
{
    return (multiplexer->readBit(startCalibrationButtonBit) > calibrationButtonThreshold);
}

void Bar::moveToPosition(BarPosition requestedPosition)
{
    if (hasReachedPosition(requestedPosition)) {
        // NOTE: Calling stop() could be optimized by using a boolean variable!
        motor->stop();
        return;
    }

    bool isRequestedPositionTowardsStart = (requestedPosition < currentPosition);
    bool isRequestedPositionTowardsEnd = !isRequestedPositionTowardsStart;

    if (isRequestedPositionTowardsStart && hasReachedStart()) {
        motor->stop();
        return;
    }

    if (isRequestedPositionTowardsEnd && hasReachedEnd()) {
        motor->stop();
        return;
    }

    if (isRequestedPositionTowardsStart) {
        motor->rotateLeft();

        if (positionMeterValue() <= positionMeterThreshold && !hasAlreadyMeteredPosition) {
            currentPosition--;
            hasAlreadyMeteredPosition = true;
        } else if (positionMeterValue() > positionMeterThreshold){
            hasAlreadyMeteredPosition = false;
        }

        return;
    }

    if (isRequestedPositionTowardsEnd) {
        motor->rotateRight();

        if (positionMeterValue() <= positionMeterThreshold && !hasAlreadyMeteredPosition) {
            currentPosition++;
            hasAlreadyMeteredPosition = true;
        } else if (positionMeterValue() > positionMeterThreshold){
            hasAlreadyMeteredPosition = false;
        }

        return;
    }
}

AnalogValue Bar::positionMeterValue()
{
    return multiplexer->readBit(positionMeterBit);
}

bool Bar::hasReachedPosition(BarPosition position)
{
    return (currentPosition == position);
}

bool Bar::hasReachedStart()
{
    return (currentPosition <= startPosition);
}

bool Bar::hasReachedEnd()
{
    return (currentPosition >= endPosition);
}

void Bar::moveToEndPosition()
{
    moveToPosition(endPosition);
}

BarPosition Bar::getPosition()
{
    return currentPosition;
}
