#pragma once

#include <stdint.h>
#include "Bar.h"

namespace TeamShapeshift { namespace SlaveFirmware
{
    class BarGrid
    {
        private:
            uint16_t columns;
            uint16_t rows;
            uint16_t lastElementIndex = 0;

            // Bar elements[rows * columns];
            // Bar *elements[10];
            Bar *elements;

        public:
            BarGrid(uint16_t rows, uint16_t columns);
            void addElement(Bar &);
    };
}}
