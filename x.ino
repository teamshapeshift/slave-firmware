#define leftButton 9
#define rightButton 8
#define endSwitch 10
#define speedMeter A0
#define pwmPin 5
#define motorPin1 6
#define motorPin2 7
int height;
int leftButtonVal, rightButtonVal, endSwitchVal, speedMeterVal;

boolean goingUp;

int speedMeterValTemp;

void setup() {
  Serial.begin(9600);
  pinMode(leftButton, INPUT_PULLUP);
  pinMode(rightButton, INPUT_PULLUP);
  pinMode(endSwitch, INPUT_PULLUP);
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);
  pinMode(pwmPin, OUTPUT);

  analogWrite(pwmPin, 255);

  getSensorVal();
  Serial.println(endSwitchVal);
  while ( endSwitchVal != 0) {
    digitalWrite(motorPin1, LOW) ;
    digitalWrite(motorPin2, HIGH) ;
    Serial.println(endSwitchVal);
    getSensorVal();
  }
  height = 0;

}

void loop() {
  analogWrite(pwmPin, 255);
  getButtons();
  getSensorVal();

  while (leftButtonVal == 0 && height < 28 ) {
    digitalWrite(motorPin1, HIGH) ;
    digitalWrite(motorPin2, LOW) ;
    getButtons();
    getSensorVal();
    goingUp = true;
    heightFunc();
  }

  while (rightButtonVal == 0 && height > 10  ) {
    digitalWrite(motorPin1, LOW) ;
    digitalWrite(motorPin2, HIGH) ;
    getButtons();
    getSensorVal();

    goingUp = false;
    heightFunc();
  }

  digitalWrite(motorPin1, HIGH) ;
  digitalWrite(motorPin2, HIGH) ;
}


void heightFunc() {
  if (endSwitchVal == 0) {
    height = 0;
  }

  if (speedMeterVal != 0 && speedMeterValTemp == 0) {
    if (goingUp) {
      height++;
    }
    else {
      height--;
    }
    Serial.print("Hoogte: ");
    Serial.println(height);
  }
  speedMeterValTemp = 10;
  if ( speedMeterVal == 0 ) {
    speedMeterValTemp = speedMeterVal;
  }
}

void getButtons() {
  leftButtonVal = digitalRead(leftButton);
  rightButtonVal = digitalRead(rightButton);
  //Serial.println(leftButtonVal);
  //Serial.println(rightButtonVal);
}

void getSensorVal() {
  endSwitchVal = digitalRead(endSwitch);
  speedMeterVal = analogRead(speedMeter);
}
