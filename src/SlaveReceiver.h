#pragma once

#include "settings.h"

class SlaveReceiver
{
    private:
        I2cBus i2cBus;

    public:
        static BarPosition requestedBarPositions[BAR_AMOUNT];

        SlaveReceiver(I2cBus);

        void listen();
        BarPosition getRequestedBarPosition(uint8_t barIndex);
};
