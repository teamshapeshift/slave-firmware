#include "settings.h"

I2cBus i2cBus = 9;

Pin multiplexSelectPins[3] = {8, 10, 11};
Pin multiplexInputPin = A0;

Pin shiftRegisterDataPin = 2;
Pin shiftRegisterClockPin = 4;
Pin shiftRegisterLatchPin = 7;
Pin shiftRegisterOutputEnablePin = 12;
Pin shiftRegisterMasterResetPin = 13;
ShiftBit shiftRegisterBitAmount = 8;

AnalogValue calibrationButtonThreshold = 400;
AnalogValue positionMeterThreshold = 800;
