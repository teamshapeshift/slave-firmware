#include <Arduino.h>
#include <Shifty.h>
#include <Multiplexer.h>

#include "settings.h"
#include "SlaveReceiver.h"
#include "Bar.h"
#include "BarMotor.h"

using namespace TeamShapeshift::SlaveFirmware;
using PedZed::ArduinoUtils::Multiplexer;

void setup()
{
    Serial.begin(9600);
    Serial.println("Slave program started.");
    Serial.print("I2C bus: ");
    Serial.println(i2cBus);

    Multiplexer multiplexer(multiplexSelectPins, multiplexInputPin);

    Shifty shiftRegister;
    shiftRegister.setBitCount(shiftRegisterBitAmount);
    shiftRegister.setPins(
        shiftRegisterDataPin,
        shiftRegisterClockPin,
        shiftRegisterLatchPin
    );
    pinMode(shiftRegisterOutputEnablePin, OUTPUT);
    pinMode(shiftRegisterMasterResetPin, OUTPUT);
    digitalWrite(shiftRegisterOutputEnablePin, LOW);
    digitalWrite(shiftRegisterMasterResetPin, HIGH);

    BarMotor barMotors[BAR_AMOUNT] = {
        BarMotor(&shiftRegister, 0, 1),
        BarMotor(&shiftRegister, 2, 3),
        BarMotor(&shiftRegister, 4, 5),
        BarMotor(&shiftRegister, 6, 7),
    };

    Bar bars[BAR_AMOUNT] = {
        Bar(&barMotors[0], &multiplexer, 0, 1),
        Bar(&barMotors[1], &multiplexer, 2, 3),
        Bar(&barMotors[2], &multiplexer, 4, 5),
        Bar(&barMotors[3], &multiplexer, 6, 7),
    };

    SlaveReceiver slaveReceiver(i2cBus);
    slaveReceiver.listen();

    for (uint8_t barIndex = 0; barIndex < BAR_AMOUNT; barIndex++) {
        bars[barIndex].calibrate();
    }

    while (true) {
        multiplexer.update();

        for (uint8_t barIndex = 0; barIndex < BAR_AMOUNT; barIndex++) {
            BarPosition requestedPosition = slaveReceiver.getRequestedBarPosition(barIndex);
            bars[barIndex].moveToPosition(requestedPosition);

            Serial.print(bars[barIndex].getPosition());
            Serial.print("\t");
        }

        Serial.println();

        // delay(100);
    }
}

// DO NOT EDIT THIS. LOOP IN SETUP.
void loop() {}
