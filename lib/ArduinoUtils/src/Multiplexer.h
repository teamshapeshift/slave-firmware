#pragma once

#include <stdint.h>

namespace PedZed { namespace ArduinoUtils
{
    using Pin = uint8_t;
    using MultiplexBit = uint8_t;

    /**
     * Multiplexer
     *
     * Tested for 74HC4051N
     *
     * Based on https://learn.sparkfun.com/tutorials/multiplexer-breakout-hookup-guide
     */
    class Multiplexer
    {
        private:
            const static uint8_t SELECT_PIN_AMOUNT = 3;
            const static uint8_t BIT_AMOUNT = 8;

            Pin *selectPins;
            Pin inputPin;

            uint16_t values[BIT_AMOUNT];

        public:
            Multiplexer(Pin *selectPins, Pin inputPin);
            ~Multiplexer();

            void update();

            uint16_t readBit(MultiplexBit bit);
    };
}}
