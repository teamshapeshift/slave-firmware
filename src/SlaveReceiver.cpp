#include <Wire.h>
#include "SlaveReceiver.h"

BarPosition SlaveReceiver::requestedBarPositions[BAR_AMOUNT];

SlaveReceiver::SlaveReceiver(I2cBus bus)
    : i2cBus(bus)
{
    Wire.begin(bus);

    for (uint8_t barIndex = 0; barIndex < BAR_AMOUNT; barIndex++) {
        SlaveReceiver::requestedBarPositions[barIndex] = 0;
    }
}

void globalOnReceiveEventThatShouldNotBeGlobal(int byteCount)
{
    for (uint8_t barIndex = 0; Wire.available() > 0 && barIndex <= BAR_AMOUNT; barIndex++) {
        char receivedChar = Wire.read();
        BarPosition barPosition = (int) receivedChar;

        SlaveReceiver::requestedBarPositions[barIndex] = barPosition;

        Serial.print("barPosition: ");
        Serial.print(barPosition);
        Serial.println();
    }
}

void SlaveReceiver::listen()
{
    Wire.onReceive(globalOnReceiveEventThatShouldNotBeGlobal);
}

BarPosition SlaveReceiver::getRequestedBarPosition(uint8_t barIndex)
{
    return SlaveReceiver::requestedBarPositions[barIndex];
}
